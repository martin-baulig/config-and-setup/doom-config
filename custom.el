(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(eglot-confirm-server-edits nil nil nil "Customized with use-package eglot")
 '(haskell-process-args-stack-ghci
   '("--ghci-options=-ferror-spans -XCPP" "--no-build" "--no-load" "--work-dir" ".stack-emacs"))
 '(lsp-haskell-server-args '("-l" "/tmp/hls.log" "-d"))
 '(package-selected-packages '(eglot))
 '(safe-local-variable-values
   '((haskell-process-use-ghci . t)
     (haskell-indent-spaces . 4))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
